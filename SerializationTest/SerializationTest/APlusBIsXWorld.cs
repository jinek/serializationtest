﻿using System.Threading;

namespace SerializationTest
{
    /// <summary>
    ///     Объект инкапсулирует некоторую математику
    /// </summary>
    internal class APlusBIsXWorld
    {
        private const string World10FileName = "ten";
        private const string World20FileName = "twenty";
        private readonly int _x;

        private APlusBIsXWorld(int x)
        {
            //Configuring simialation
            Thread.Sleep(2000);
            if (x > 15) x = 10;
            _x = x;
        }

        /// <summary>
        ///     Посчитать функцию в этом мире
        /// </summary>
        public int CalculateA(int b)
        {
            return _x - b;
        }

        /// <summary>
        ///     Получить все возможные конфигурации математических миров
        /// </summary>
        public static APlusBIsXWorld[] GetPossibleWorlds()
        {
            return new[]
            {
                APlusBIsXWorldExtensions.Deserialize(World10FileName) ?? new APlusBIsXWorld(10).Serialize(World10FileName),
                APlusBIsXWorldExtensions.Deserialize(World20FileName) ?? new APlusBIsXWorld(20).Serialize(World20FileName),
            };
        }
    }
}