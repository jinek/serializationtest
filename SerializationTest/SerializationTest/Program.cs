﻿using System;

namespace SerializationTest
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Please, enter number");

            int number;

            {
                string str;
                do
                {
                    str = Console.ReadLine();
                } while (str == string.Empty || !int.TryParse(str, out number));
            }

            Console.WriteLine("In possible APlusBIsX worlds calculations of A are:");

            foreach (var xWorld in APlusBIsXWorld.GetPossibleWorlds())
                Console.WriteLine(xWorld.CalculateA(number));

            Console.ReadLine();
        }
    }
}